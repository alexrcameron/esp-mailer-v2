#include <ESP8266WiFi.h>
#include "ESP8266SMTP_insecure.h"
#include "config.h"

char* recipient_email = config_emailRecipient;
char* email_body = "Email Body Text";

void setup()
{
  Serial.begin(115200);
  delay(2000);
  
  ConnectToWifi();

  SetupEmail();

  SendEmail(recipient_email, email_body);
}

void loop()
{}
