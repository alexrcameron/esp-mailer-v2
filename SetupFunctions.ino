const char* ssid = config_SSID;
const char* password = config_networkPassword;

uint8_t connection_state = 0;           // Connected to WIFI or not
uint16_t reconnect_interval = 10000;    // If not connected wait time to try again

const char* email_address = config_emailLogin;
const char* email_password =  config_emailPassword;

uint8_t WiFiConnect(const char* ssID, const char* nPassword)
{
  static uint16_t attempt = 0;
  Serial.print("Connecting to ");
  Serial.println(ssID);
  WiFi.begin(ssID, nPassword);

  uint8_t i = 0;
  while(WiFi.status() != WL_CONNECTED && i++ < 50) {
    delay(200);
    Serial.print(".");
  }
  ++attempt;
  Serial.println("");
  if(i == 51) {
    Serial.print(F("Connection: TIMEOUT on attempt: "));
    Serial.println(attempt);
    if(attempt % 2 == 0)
      Serial.println(F("Check if access point available or SSID and Password are correct\r\n"));
    return false;
  }
  Serial.println(F("Connection: ESTABLISHED"));
  Serial.print(F("Got IP address: "));
  Serial.println(WiFi.localIP());
  return true;
}

void Awaits(uint16_t interval)
{
  uint32_t ts = millis();
  while(!connection_state){
    delay(50);
    if(!connection_state && millis() > (ts + interval)){
      connection_state = WiFiConnect(ssid, password);
      ts = millis();
    }
  }
}


void ConnectToWifi(){
  connection_state = WiFiConnect(ssid, password);

  if(!connection_state) {         // if not connected to WIFI
    Awaits(reconnect_interval);         // constantly trying to connect
  }
}

void SetupEmail(){
  SMTP.setEmail(email_address)
    .setPassword(email_password)
    .Subject("ESP8266SMTP Gmail test")
    .setFrom("ESP8266SMTP")
    .setForGmail();           // sets port to 465 and setServer("smtp.gmail.com");
}

void SendEmail(char* To, char* Message){
  if(SMTP.Send(To, Message)) {
    Serial.println(F("Message sent"));
  } else {
    Serial.print(F("Error sending message: "));
    Serial.println(SMTP.getError());
  }
}
